function fish_prompt --description 'Write out the prompt'
	
	# Just calculate these once, to save a few cycles when displaying the prompt
	if not set -q __fish_prompt_hostname
		set -g __fish_prompt_hostname (hostname|cut -d . -f 1)
	end

	if not set -q __fish_prompt_normal
		set -g __fish_prompt_normal (set_color normal)
	end

	if not set -q __fish_prompt_cwd
		set -g __fish_prompt_cwd (set_color brown)
	end

	set -g __at_separator_color (set_color brown)
	set -g __host_color (set_color green)

	__informative_git_prompt

	echo -n -s "$USER" "$__at_separator_color" '@' "$__host_color" "$__fish_prompt_hostname" ':' "$__fish_prompt_cwd" (prompt_pwd) "$__fish_prompt_normal" '$ '


end
